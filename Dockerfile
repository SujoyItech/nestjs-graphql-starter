FROM node:17-bullseye As development

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install



FROM node:17-bullseye as builder

ENV PORT=3000
ENV NODE_ENV=production

RUN mkdir -p /var/www/nft-backend
WORKDIR /var/www/nft-backend

COPY package*.json ./
COPY yarn.lock ./

# clear application caching
RUN yarn cache clean
# install all dependencies
RUN yarn install

COPY . .

# build application
RUN yarn build


FROM node:17-bullseye as production

# Install PM2
RUN yarn global add pm2

# Set working directory
RUN mkdir -p /var/www/nft-backend
WORKDIR /var/www/nft-backend

ENV PATH /var/www/nft-backend/node_modules/.bin:$PATH
# create user with no password
RUN adduser --disabled-password nftdemon

# Copy existing application directory contents
COPY --from=builder /var/www/nft-backend/package*.json ./
COPY --from=builder /var/www/nft-backend/yarn.lock ./
COPY --from=builder /var/www/nft-backend/.env ./
COPY --from=builder /var/www/nft-backend/pm2.yaml ./
COPY --from=builder /var/www/nft-backend/dist ./dist
COPY --from=builder /var/www/nft-backend/contracts ./contracts
COPY --from=builder /var/www/nft-backend/prisma ./prisma
COPY --from=builder /var/www/nft-backend/public ./public

# grant a permission to the application
RUN chown -R nftdemon:nftdemon /var/www/nft-backend
USER nftdemon

# clear application caching
RUN yarn cache clean
# install production dependencies
RUN yarn install --production

EXPOSE $PORT
# start run in production environment
CMD [ "yarn", "pm2:delete" ]
CMD [ "yarn", "pm2:start" ]