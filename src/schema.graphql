# ------------------------------------------------------
# THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
# ------------------------------------------------------

type Bid {
  amount: Float!

  """Identifies the date and time when the object was created."""
  createdAt: Date!
  id: Int!
  item: Item
  itemId: Int!
  sender: User
  senderId: Int!
  status: String!

  """Identifies the date and time when the object was last updated."""
  updatedAt: Date!
}

type Category {
  """Identifies the date and time when the object was created."""
  createdAt: Date!
  id: Int!
  image: String!
  items: [Item!]
  title: String!

  """Identifies the date and time when the object was last updated."""
  updatedAt: Date!
}

input CategoryCreateInput {
  image: String
  title: String!
}

type Collection {
  author: User
  authorId: Int!
  contractAddress: String
  contractSymbol: String
  coverImage: String

  """Identifies the date and time when the object was created."""
  createdAt: Date!
  description: String
  id: Int!
  image: String
  items: [Item!]
  name: String!
  note: String
  royalties: Float!
  status: String!

  """Identifies the date and time when the object was last updated."""
  updatedAt: Date!
  volumeTraded: Int!
}

type CollectionConnection {
  edges: [CollectionEdge!]
  pageInfo: PageInfo!
  totalCount: Int!
}

type CollectionEdge {
  cursor: String!
  node: Collection!
}

input CollectionOrder {
  direction: OrderDirection!
  field: CollectionOrderField!
}

"""Properties by which collection connections can be ordered."""
enum CollectionOrderField {
  createdAt
  id
  name
}

type CollectionWithMeta {
  collection: Collection!
  itemCount: Int!
}

input CreateDto {
  auctionEndAt: Date
  categoryId: Int!
  collectionId: Int!
  description: String
  image: String!
  levels: [RangeDto!]
  mediaPath: String!
  name: String!
  price: Float!
  properties: [PropertyDto!]
  stats: [RangeDto!]
}

"""Date custom scalar type"""
scalar Date

type FileObject {
  name: String!
  type: String!
  url: String!
  variants: [FileVariant!]
}

type FileVariant {
  type: String!
  url: String!
}

type History {
  amount: String!

  """Identifies the date and time when the object was created."""
  createdAt: Date!
  from: User
  fromId: Int!
  hash: String
  id: Int!
  isValid: Boolean!
  item: Item
  itemId: Int!
  to: User
  toId: Int
  type: String!

  """Identifies the date and time when the object was last updated."""
  updatedAt: Date!
}

type Item {
  auctionEndAt: Date
  bids: [Bid!]
  category: Category
  categoryId: Int!
  collection: Collection
  collectionId: Int!
  createdAt: Date!
  creator: User
  creatorId: Int!
  currentOwner: User
  currentOwnerId: Int!
  description: String
  externalUrl: String
  histories: [History!]
  id: Int!
  image: String
  ipfsHash: String
  levels: [ItemLevel!]
  likeCount: Int!
  likes: [Like!]
  mediaPath: String
  mintedAt: Date
  name: String!
  price: Float!
  prices: [Price!]
  properties: [ItemProperty!]
  stats: [ItemStat!]
  status: String!
  tokenId: Int
  unlockContentUrl: String
  updatedAt: Date
  viewCount: Int!
}

type ItemConnection {
  edges: [ItemEdge!]
  pageInfo: PageInfo!
  totalCount: Int!
}

type ItemEdge {
  cursor: String!
  node: Item!
}

type ItemLevel {
  id: ID!
  item: Item
  itemId: Int!
  name: String!
  value: Float!
  valueof: Float!
}

input ItemOrder {
  direction: OrderDirection!
  field: ItemOrderField!
}

"""Properties by which Item connections can be ordered."""
enum ItemOrderField {
  id
  mintedAt
  name
  price
  tokenId
  view
}

type ItemProperty {
  id: ID!
  item: Item
  itemId: Int!
  name: String!
  type: String!
}

type ItemStat {
  id: ID!
  item: Item
  itemId: Int!
  name: String!
  value: Float!
  valueof: Float!
}

type Like {
  id: ID!
  item: Item
  itemId: Int!
  user: User
  userId: Int!
}

input LoginInput {
  password: String!
  username: String!
}

type Mutation {
  StaffLogin(data: StaffLoginInput!): Token!
  changePassword(data: ResetPasswordInput!): ResponseMessageWithStatusModel!
  createCategory(data: CategoryCreateInput!): Category!
  createCollection(data: collectionInput!): Collection!
  createRole(data: roleInput!): Role!
  createStaff(data: StaffCreateInput!): Staff!
  createWallet: String!
  deleteCategory(id: Int!): Category!
  deleteRole(id: Int!): Role!
  deleteStaff(id: Int!): Staff!
  deployCollection(collectionId: Int!): String!
  enableAuction(date: Date!, itemId: Int!): Boolean!
  login(data: LoginInput!): Token!
  makeWin: Boolean!
  placeBid(amount: Float!, itemId: Int!): Bid!
  publishItem(itemId: Int!): String!
  refreshToken(token: String!): Token!
  removeMyBid(itemId: Int!): Boolean!
  sendForgetPasswordMail(email: String!): ResponseMessageWithStatusModel!
  sendVerificationMail: ResponseMessageWithStatusModel!
  signup(data: SignupInput!): Token!
  storeItem(data: CreateDto!): Item!
  toggleLike(itemId: Int!, liked: Boolean!): Boolean!
  updateCategory(data: CategoryCreateInput!, id: Int!): Category!
  updatePassword(data: UpdatePasswordInput!): ResponseMessageWithStatusModel!
  updateProfile(data: UpdateProfileInput!): ResponseMessageWithStatusModel!
  updateRole(data: roleInput!, id: Int!): Role!
  updateStaff(data: StaffUpdateInput!, id: Int!): Staff!
  updateStaffPassword(data: UpdatePasswordInput!): ResponseMessageWithStatusModel!
  updateStaffProfile(data: StaffUpdateInput!): ResponseMessageWithStatusModel!
  uploadFile(file: Upload!): FileObject!
  verifyUserEmail(code: Int!): ResponseMessageWithStatusModel!
}

"""
Possible directions in which to order a list of items when provided an `orderBy` argument.
"""
enum OrderDirection {
  asc
  desc
}

type PageInfo {
  endCursor: String
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
}

type Price {
  amount: Float!

  """Identifies the date and time when the object was created."""
  createdAt: Date!
  id: Int!
  item: Float
  itemId: Int!

  """Identifies the date and time when the object was last updated."""
  updatedAt: Date!
}

input PropertyDto {
  name: String!
  type: String!
}

type Query {
  getCategories: [Category!]!
  getCollection(address: String!): CollectionWithMeta!
  getRoles: [Role!]!
  getSettings: SettingsDto!
  getStaff: [Staff!]!
  listFile: [FileObject!]!
  listItems(after: String, before: String, categoryId: Int, collectionId: Int, first: Int, last: Int, orderBy: ItemOrder, query: String, skip: Int): ItemConnection!
  me: User!
  myCollections(after: String, before: String, first: Int, last: Int, orderBy: CollectionOrder, query: String, skip: Int): CollectionConnection!
  ownedItems(after: String, before: String, categoryId: Int, collectionId: Int, first: Int, last: Int, orderBy: ItemOrder, query: String, skip: Int): ItemConnection!
  singleItem(contractAddress: String!, token: Int!): Item!
  staff: Staff!
  walletBalance: Float!
}

input RangeDto {
  name: String!
  value: Float!
  valueof: Float!
}

input ResetPasswordInput {
  code: String!
  email: String!
  password: String!
  passwordConfirm: String!
}

type ResponseMessageWithStatusModel {
  """message"""
  message: String!

  """success"""
  success: Boolean!
}

type Role {
  """Identifies the date and time when the object was created."""
  createdAt: Date!
  id: Int!
  name: String!
  permissions: String

  """Identifies the date and time when the object was last updated."""
  updatedAt: Date!
}

type Section {
  description: String
  title: String!
}

type Sections {
  category: Section!
}

type SettingsDto {
  cur: String!
  sections: Sections!
}

input SignupInput {
  email: String!
  name: String!
  password: String!
  phone: String
  username: String!
}

type Staff {
  avatar: String

  """Identifies the date and time when the object was created."""
  createdAt: Date!
  email: String!
  id: Int!
  name: String!
  phone: String
  roleId: Int!

  """Identifies the date and time when the object was last updated."""
  updatedAt: Date!
  username: String!
}

input StaffCreateInput {
  avatar: String
  email: String!
  name: String!
  password: String!
  phone: String
  roleId: Int
  username: String!
}

input StaffLoginInput {
  password: String!
  username: String!
}

input StaffUpdateInput {
  avatar: String
  email: String
  name: String
  password: String
  phone: String
  roleId: Int
  username: String
}

type Token {
  """JWT access token"""
  accessToken: String!

  """JWT expiration time"""
  expireAt: Date!

  """JWT refresh token"""
  refreshToken: String!
}

input UpdatePasswordInput {
  oldPassword: String!
  password: String!
  passwordConfirm: String!
}

input UpdateProfileInput {
  avatar: String
  email: String!
  name: String
  phone: String
  username: String!
}

"""The `Upload` scalar type represents a file upload."""
scalar Upload

type User {
  avatar: String
  collections: [Collection!]

  """Identifies the date and time when the object was created."""
  createdAt: Date!
  createdItems: [Item!]
  email: String!
  emailVerifiedAt: Date
  id: Int!
  isEmailVerified: Boolean!
  name: String!
  ownedItems: [Item!]
  phone: String
  resetCode: String!

  """Identifies the date and time when the object was last updated."""
  updatedAt: Date!
  username: String!
  walletAddress: String
}

input collectionInput {
  coverImage: String
  description: String
  image: String
  name: String!
  royalties: Float!
}

input roleInput {
  name: String!
  permissions: String
}
