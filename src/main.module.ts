import { GraphQLModule } from '@nestjs/graphql';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { GraphqlConfig } from './configs/config.interface';
import { PrismaModule } from 'nestjs-prisma';
import { DateScalar } from './libs/graphql/scalars/date.scalar';
import { FilesystemModule } from './app/filesystem/filesystem.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { resolve } from 'path';
import { BullModule } from '@nestjs/bull';
import { QueueOptions } from 'bull';
import { NotificationModule } from './libs/notification/notification.module';
import { MailModule } from './libs/mail/mail.module';

import { NestConfig } from './configs/nest.config';
import { CorsConfig } from './configs/cors.config';
import { GraphQLConfig } from './configs/graphql.config';
import { JWTConfig } from './configs/security.config';
import { QueueConfig } from './configs/queue.config';
import { FilesystemConfig } from './configs/filesystem.config';
import { MailConfig } from './configs/mail.config';
import { AppConfig } from './configs/app.config';
import { ServicesConfig } from './configs/services.config';

import { CacheModule } from './libs/cache/cache.module';
import { LogModule } from './libs/log/log.module';
import { Module } from '@nestjs/common';
import { CacheConfig } from './configs/cache.config';
import { AuthConfig } from './configs/auth.config';
import { AppModule } from './app/app.module';
import {ScheduleModule} from "@nestjs/schedule";

@Module({
  imports: [
    // Core Modules
    ConfigModule.forRoot({
      isGlobal: true,
      load: [
        NestConfig,
        CorsConfig,
        AppConfig,
        GraphQLConfig,
        AuthConfig,
        JWTConfig,
        QueueConfig,
        FilesystemConfig,
        MailConfig,
        CacheConfig,
        ServicesConfig,
      ],
    }),
    ServeStaticModule.forRoot({
      rootPath: resolve('public'),
      exclude: ['/graphql*'],
    }),
    GraphQLModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const graphqlConfig = configService.get<GraphqlConfig>('graphql');
        return {
          installSubscriptionHandlers: true,
          buildSchemaOptions: {
            numberScalarMode: 'integer',
          },
          sortSchema: graphqlConfig.sortSchema,
          autoSchemaFile:
            graphqlConfig.schemaDestination || './src/schema.graphql',
          debug: graphqlConfig.debug,
          playground: graphqlConfig.playgroundEnabled,
          formatError: graphqlConfig.formatError,
          context: ({ req }) => ({ req }),
        };
      },
      inject: [ConfigService],
    }),
    FilesystemModule,
    CacheModule,
    PrismaModule.forRoot({
      isGlobal: true,
    }),
    BullModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        return configService.get<QueueOptions>('queue');
      },
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
    LogModule,
    MailModule,
    NotificationModule,

    // Application Modules
    AppModule,
  ],
  controllers: [],
  providers: [DateScalar],
})
export class MainModule {}
