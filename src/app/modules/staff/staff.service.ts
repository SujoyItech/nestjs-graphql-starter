import { ConflictException, Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { Prisma } from '@prisma/client';
import { StaffCreateInput, StaffUpdateInput } from './dto/staff.input';
import { Staff } from './staff.model';
import { PasswordService } from 'src/libs/auth/password.service';
import { UpdatePasswordInput, UpdateProfileInput } from '../user/dto/user.dto';
import { responseMessageWithStatus } from '../../models/dto/response-message-with-status';
import { compare } from 'bcryptjs';

@Injectable()
export class StaffService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly passwordService: PasswordService,
  ) {}

  async updateStaffProfile(payload: StaffUpdateInput, staff) {
    try {
      const response = await this.prisma.staff.update({
        where: {
          id: staff.id,
        },
        data: {
          ...payload,
        },
      });
      return response
        ? responseMessageWithStatus(true, 'Profile updated successfully!')
        : responseMessageWithStatus(false, 'Profile update failed.');
    } catch (e) {
      return responseMessageWithStatus(false, e.message);
    }
  }

  async updateStaffPassword(payload: UpdatePasswordInput, staff) {
    try {
      const validatePassword = await compare(
        payload.oldPassword,
        staff.password,
      );
      if (validatePassword) {
        const hashedPassword = await this.passwordService.hashPassword(
          payload.password,
        );
        const response = await this.prisma.staff.update({
          where: {
            id: staff.id,
          },
          data: {
            password: hashedPassword,
          },
        });
        return response
          ? responseMessageWithStatus(true, 'Password updated successfully!')
          : responseMessageWithStatus(false, 'Password update failed.');
      } else {
        return responseMessageWithStatus(false, 'Incorrect old password!');
      }
    } catch (e) {
      return responseMessageWithStatus(false, e.message);
    }
  }

  async getStaffLists(): Promise<Staff[]> {
    return await this.prisma.staff.findMany({
      include: {
        role: true,
      },
    });
  }

  async getStaffByID(id: number): Promise<Staff> {
    return await this.prisma.staff.findUnique({
      where: {
        id: id,
      },
      include: {
        role: true,
      },
    });
  }

  async createStaff(payload: StaffCreateInput): Promise<Staff> {
    const hashedPassword = await this.passwordService.hashPassword(
      payload.password,
    );

    try {
      const staff = await this.prisma.staff.create({
        data: {
          ...payload,
          password: hashedPassword,
        },
      });
      return staff;
    } catch (e) {
      if (
        e instanceof Prisma.PrismaClientKnownRequestError &&
        e.code === 'P2002'
      ) {
        throw new ConflictException(`Email ${payload.email} already used.`);
      } else {
        throw new Error(e);
      }
    }
  }

  async updateStaff(id, payload: StaffUpdateInput): Promise<Staff> {
    try {
      const staff = await this.prisma.staff.update({
        where: {
          id: id,
        },
        data: {
          ...payload,
        },
      });
      return staff;
    } catch (e) {
      throw new Error(e);
    }
  }

  async deleteStaff(id): Promise<Staff> {
    try {
      const staff = await this.prisma.staff.delete({
        where: {
          id: id,
        },
      });
      return staff;
    } catch (e) {
      throw new Error(e);
    }
  }
}
