import { ObjectType, HideField } from '@nestjs/graphql';
import { BaseModel } from '../../../libs/model/base.model';
import { AuthenticatableInterface } from '../../../libs/auth/authenticatable.interface';
import { Role } from './role/role.model';

@ObjectType()
export class Staff extends BaseModel implements AuthenticatableInterface {
  id: number;
  username: string;
  email: string;
  name: string;
  avatar?: string;
  phone?: string;
  roleId?: number;
  role?: Role;
  @HideField()
  password: string;
}
