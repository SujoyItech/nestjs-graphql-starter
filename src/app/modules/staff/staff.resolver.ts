import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { Staff } from './staff.model';
import { StaffCreateInput, StaffUpdateInput } from './dto/staff.input';
import { StaffService } from './staff.service';
import { GqlAuthGuard } from '../../../libs/auth/gql.auth.guard';
import { UserEntity } from '../../../libs/decorators/user.decorator';
import { ResponseMessageWithStatusModel } from '../../models/dto/response-message-with-status.model';
import { UpdatePasswordInput } from '../user/dto/user.dto';
import { roleInput } from './role/dto/role.input';
import { responseMessageWithStatus } from '../../models/dto/response-message-with-status';

@Resolver(() => Staff)
@UseGuards(GqlAuthGuard('staff'))
export class StaffResolver {
  constructor(private readonly staffService: StaffService) {}

  @Query(() => Staff)
  async staff(@UserEntity() staff: Staff): Promise<Staff> {
    return staff;
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async updateStaffProfile(
    @Args('data') data: StaffUpdateInput,
    @UserEntity() staff: Staff,
  ) {
    return await this.staffService.updateStaffProfile(data, staff);
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async updateStaffPassword(
    @Args('data') data: UpdatePasswordInput,
    @UserEntity() staff: Staff,
  ) {
    return await this.staffService.updateStaffPassword(data, staff);
  }

  @Query(() => [Staff])
  async getStaffLists(): Promise<Staff[]> {
    return await this.staffService.getStaffLists();
  }

  @Query(() => Staff)
  async getStaff(@Args({ name: 'id', type: () => Int }) id): Promise<Staff> {
    return await this.staffService.getStaffByID(id);
  }

  @Mutation(() => Staff)
  async createStaff(@Args('data') data: StaffCreateInput): Promise<Staff> {
    return await this.staffService.createStaff(data);
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async updateStaff(
    @Args({ name: 'id', type: () => Int }) id: number,
    @Args('data') data: StaffUpdateInput,
  ): Promise<ResponseMessageWithStatusModel> {
    try {
      const response = await this.staffService.updateStaff(id, data);
      return response
        ? responseMessageWithStatus(true, 'Staff updated successfully!')
        : responseMessageWithStatus(false, 'Staff update failed.');
    } catch (error) {
      return responseMessageWithStatus(false, error.message);
    }
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async deleteStaff(@Args({ name: 'id', type: () => Int }) id): Promise<Staff> {
    return await this.staffService.deleteStaff(id);
  }
}
