import {ConflictException, Injectable} from '@nestjs/common';
import {PrismaService} from 'nestjs-prisma';
import {CategoryCreateInput} from './dto/category.input';
import {Prisma} from '@prisma/client';
import {Category} from '../../../models/category.model';

@Injectable()
export class CategoryService {
  constructor(private readonly prisma: PrismaService) {}

  async getCategories(): Promise<Category[]> {
    return await this.prisma.category.findMany();
  }

  async createCategory(payload: CategoryCreateInput): Promise<Category> {
    try {
      return await this.prisma.category.create({
        data: {
          ...payload,
        },
      });
    } catch (e) {
      if (
        e instanceof Prisma.PrismaClientKnownRequestError &&
        e.code === 'P2002'
      ) {
        throw new ConflictException(`Category ${payload.title} already used.`);
      } else {
        throw new Error(e);
      }
    }
  }

  async updateCategory(id, payload: CategoryCreateInput): Promise<Category> {
    try {
      return await this.prisma.category.update({
        where: {
          id: id,
        },
        data: {
          ...payload,
        },
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  async deleteCategory(id): Promise<Category> {
    try {
      return await this.prisma.category.delete({
        where: {
          id: id,
        },
      });
    } catch (e) {
      throw new Error(e);
    }
  }
}
