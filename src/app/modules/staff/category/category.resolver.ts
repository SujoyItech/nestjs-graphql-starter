import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { CategoryCreateInput } from './dto/category.input';
import { UseGuards } from '@nestjs/common';
import { CategoryService } from './category.service';
import { RolePermissionGuard } from '../../../guards/role-permission.guard';
import { Category } from '../../../models/category.model';
import { GqlAuthGuard } from '../../../../libs/auth/gql.auth.guard';

@Resolver(() => Category)
export class CategoryResolver {
  constructor(private readonly categoryService: CategoryService) {}

  @Query(() => [Category])
  async getCategories(): Promise<Category[]> {
    return await this.categoryService.getCategories();
  }

  @UseGuards(GqlAuthGuard('staff'))
  @UseGuards(new RolePermissionGuard('perm1'))
  @Mutation(() => Category)
  async createCategory(
    @Args('data') data: CategoryCreateInput,
  ): Promise<Category> {
    return await this.categoryService.createCategory(data);
  }

  @UseGuards(GqlAuthGuard('staff'))
  @UseGuards(new RolePermissionGuard('perm1'))
  @Mutation(() => Category)
  async updateCategory(
    @Args({ name: 'id', type: () => Int }) id: number,
    @Args('data') data: CategoryCreateInput,
  ): Promise<Category> {
    return await this.categoryService.updateCategory(id, data);
  }

  @UseGuards(GqlAuthGuard('staff'))
  @UseGuards(new RolePermissionGuard('perm1'))
  @Mutation(() => Category)
  async deleteCategory(
    @Args({ name: 'id', type: () => Int }) id: number,
  ): Promise<Category> {
    return await this.categoryService.deleteCategory(id);
  }
}
