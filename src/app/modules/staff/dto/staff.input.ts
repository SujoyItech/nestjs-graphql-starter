import { IsAlphanumeric, IsEmail, IsEmpty, MinLength } from 'class-validator';
import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class StaffCreateInput {
  @Field()
  @IsAlphanumeric()
  username: string;

  @IsEmail()
  email: string;

  @Field()
  name: string;

  @Field({ nullable: true })
  avatar: string;

  @Field({ nullable: true })
  phone: string;

  @Field({ nullable: true })
  roleId: number;

  @Field()
  @MinLength(8)
  password: string;
}

@InputType()
export class StaffUpdateInput {
  @Field({ nullable: true })
  email: string;

  @Field({ nullable: true })
  name: string;

  @Field({ nullable: true })
  avatar: string;

  @Field({ nullable: true })
  username: string;

  @Field({ nullable: true })
  password: string;

  @Field({ nullable: true })
  phone: string;

  @Field({ nullable: true })
  roleId: number;
}
