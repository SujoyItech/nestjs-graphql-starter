import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { roleInput } from './dto/role.input';
import { UseGuards } from '@nestjs/common';
import { RoleService } from './role.service';
import { Role } from './role.model';
import { GqlAuthGuard } from '../../../../libs/auth/gql.auth.guard';
import { ResponseMessageWithStatusModel } from '../../../models/dto/response-message-with-status.model';
import { responseMessageWithStatus } from '../../../models/dto/response-message-with-status';
import { RoleOrder } from '../../../models/input/role-order.input';

@Resolver(() => Role)
@UseGuards(GqlAuthGuard('staff'))
export class RoleResolver {
  constructor(private readonly roleService: RoleService) {}

  @Query(() => Role)
  async getRole(
    @Args({ name: 'id', type: () => Int }) id: number | null,
  ): Promise<Role> {
    return await this.roleService.getRole(id);
  }

  @Query(() => [Role])
  async getRoles(
    @Args({
      name: 'query',
      type: () => String,
      nullable: true,
    })
    query: string,
    @Args({
      name: 'orderBy',
      type: () => RoleOrder,
      nullable: true,
    })
    orderBy: RoleOrder,
  ): Promise<Role[]> {
    return await this.roleService.getRoles(query, orderBy);
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async createRole(
    @Args('data') data: roleInput,
  ): Promise<ResponseMessageWithStatusModel> {
    try {
      const response = await this.roleService.createRole(data);
      return response
        ? responseMessageWithStatus(true, 'Role created successfully!')
        : responseMessageWithStatus(false, 'Role create failed.');
    } catch (error) {
      return responseMessageWithStatus(false, error.message);
    }
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async updateRole(
    @Args({ name: 'id', type: () => Int }) id: number,
    @Args('data') data: roleInput,
  ): Promise<ResponseMessageWithStatusModel> {
    try {
      const response = await this.roleService.updateRole(id, data);
      return response
        ? responseMessageWithStatus(true, 'Role updated successfully!')
        : responseMessageWithStatus(false, 'Role update failed.');
    } catch (error) {
      return responseMessageWithStatus(false, error.message);
    }
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async deleteRole(
    @Args({ name: 'id', type: () => Int }) id: number,
  ): Promise<ResponseMessageWithStatusModel> {
    try {
      const response = await this.roleService.deleteRole(id);
      return response
        ? responseMessageWithStatus(true, 'Role deleted successfully!')
        : responseMessageWithStatus(false, 'Role delete failed.');
    } catch (error) {
      return responseMessageWithStatus(false, error.message);
    }
  }
}
