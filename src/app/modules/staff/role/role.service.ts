import { ConflictException, Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { roleInput } from './dto/role.input';
import { Prisma } from '@prisma/client';
import { Role } from './role.model';
import { RoleOrder } from '../../../models/input/role-order.input';

@Injectable()
export class RoleService {
  constructor(private readonly prisma: PrismaService) {}

  async getRole(id?) {
    return await this.prisma.role.findUnique({
      where: {
        id: id,
      },
    });
  }

  async getRoles(query?: string, orderBy?: RoleOrder) {
    return await this.prisma.role.findMany({
      where: {
        name: query ? query : undefined,
      },
      orderBy: orderBy ? { [orderBy.field]: orderBy.direction } : undefined,
    });
  }

  async createRole(payload: roleInput): Promise<Role> {
    try {
      const role = await this.prisma.role.create({
        data: {
          ...payload,
        },
      });
      return role;
    } catch (e) {
      if (
        e instanceof Prisma.PrismaClientKnownRequestError &&
        e.code === 'P2002'
      ) {
        throw new ConflictException(`Role ${payload.name} already used.`);
      } else {
        throw new Error(e);
      }
    }
  }

  async updateRole(id, payload: roleInput): Promise<Role> {
    try {
      const role = await this.prisma.role.update({
        where: {
          id: id,
        },
        data: {
          ...payload,
        },
      });
      return role;
    } catch (e) {
      throw new Error(e);
    }
  }

  async deleteRole(id): Promise<Role> {
    try {
      const role = await this.prisma.role.delete({
        where: {
          id: id,
        },
      });
      return role;
    } catch (e) {
      throw new Error(e);
    }
  }
}
