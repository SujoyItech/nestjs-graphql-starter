import { Query, Resolver } from '@nestjs/graphql';
import { SettingsDto } from './dto/settings.dto';
import { ConfigService } from '@nestjs/config';
import { AppConfig as AppConfigInterface } from '../../../configs/config.interface';

@Resolver()
export class MiscResolver {
  constructor(private readonly configService: ConfigService) {}

  @Query(() => SettingsDto)
  getSettings(): SettingsDto {
    return this.configService.get<AppConfigInterface>('app');
  }
}
