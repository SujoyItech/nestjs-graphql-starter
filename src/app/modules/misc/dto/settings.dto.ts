import { Field, HideField, ObjectType } from '@nestjs/graphql';
import {
  AppConfig,
  SectionSettings,
  UiSection,
} from '../../../../configs/config.interface';

@ObjectType()
class Section implements UiSection {
  @Field()
  title: string;

  @Field({ nullable: true })
  description?: string;
}

@ObjectType()
class Sections implements SectionSettings {
  @Field()
  category: Section;
}

@ObjectType()
export class SettingsDto implements AppConfig {
  @HideField()
  emailVerificationEnabled: boolean;

  @Field()
  cur: string;
  @Field()
  sections: Sections;
  timeZone: string;

  @HideField()
  adminCommission: number;
  @HideField()
  adminWalletAddress: string | null;
}
