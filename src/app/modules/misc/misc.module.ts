import { Module } from '@nestjs/common';
import { MiscResolver } from './misc.resolver';

@Module({
  providers: [MiscResolver],
})
export class MiscModule {}
