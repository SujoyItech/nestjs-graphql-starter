import { LoginInput } from './dto/login.input';
import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { SignupInput } from './dto/signup.input';
import { RefreshTokenInput } from '../../../libs/auth/dto/refresh-token.input';
import { Token } from '../../../libs/auth/models/token.model';
import { ResponseMessageWithStatusModel } from '../../models/dto/response-message-with-status.model';
import { ResetPasswordInput } from '../../../libs/auth/dto/reset-password.input';
import { UserAuthService } from './user.auth.service';
import { responseMessageWithStatus } from '../../models/dto/response-message-with-status';

@Resolver('UserAuth')
export class AuthResolver {
  constructor(private readonly auth: UserAuthService) {}

  @Mutation(() => Token)
  async signup(@Args('data') data: SignupInput) {
    data.email = data.email.toLowerCase();
    return this.auth.register(data);
  }

  @Mutation(() => Token)
  async login(@Args('data') data: LoginInput) {
    return await this.auth.login(data);
  }

  @Mutation(() => Token)
  async refreshToken(@Args() { token }: RefreshTokenInput) {
    return this.auth.refreshToken(token);
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async sendForgetPasswordMail(@Args('email') email: string) {
    try {
      const response = await this.auth.sendForgetPasswordNotification({
        email,
      });
      return response
        ? responseMessageWithStatus(
            true,
            'Forget password mail sent successfully!',
          )
        : responseMessageWithStatus(false, 'Forget password mail send failed!');
    } catch (error) {
      return responseMessageWithStatus(false, error.message);
    }
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async changePassword(@Args('data') data: ResetPasswordInput) {
    try {
      const response = await this.auth.resetPassword(data);
      return response
        ? responseMessageWithStatus(true, 'Password changed successfully!')
        : responseMessageWithStatus(false, 'Password change failed!');
    } catch (error) {
      return responseMessageWithStatus(false, error.message);
    }
  }
}
