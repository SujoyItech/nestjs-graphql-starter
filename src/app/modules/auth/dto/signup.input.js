"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SignupInput = void 0;
var class_validator_1 = require("class-validator");
var graphql_1 = require("@nestjs/graphql");
var SignupInput = /** @class */ (function () {
    function SignupInput() {
    }
    SignupInput._GRAPHQL_METADATA_FACTORY = function () {
        return { username: { type: function () { return String; } }, email: { type: function () { return String; } }, name: { type: function () { return String; } }, phone: { type: function () { return String; } }, password: { type: function () { return String; } } };
    };
    __decorate([
        (0, graphql_1.Field)(),
        (0, class_validator_1.IsAlphanumeric)()
    ], SignupInput.prototype, "username");
    __decorate([
        (0, graphql_1.Field)(),
        (0, class_validator_1.IsEmail)()
    ], SignupInput.prototype, "email");
    __decorate([
        (0, graphql_1.Field)()
    ], SignupInput.prototype, "name");
    __decorate([
        (0, graphql_1.Field)({ nullable: true })
    ], SignupInput.prototype, "phone");
    __decorate([
        (0, graphql_1.Field)(),
        (0, class_validator_1.IsNotEmpty)(),
        (0, class_validator_1.MinLength)(8)
    ], SignupInput.prototype, "password");
    SignupInput = __decorate([
        (0, graphql_1.InputType)()
    ], SignupInput);
    return SignupInput;
}());
exports.SignupInput = SignupInput;
