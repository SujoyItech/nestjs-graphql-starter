import { PrismaService } from 'nestjs-prisma';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
  ConflictException,
} from '@nestjs/common';
import { PasswordService } from '../../../libs/auth/password.service';
import { SignupInput } from './dto/signup.input';
import { Prisma, User } from '@prisma/client';
import { ConfigService } from '@nestjs/config';
import { AppConfig } from 'src/configs/config.interface';
import { Token } from '../../../libs/auth/models/token.model';
import { MailVerificationNotification } from '../../notifications/mailverification.notification';
import { NotificationService } from '../../../libs/notification/notification.service';
import { ResetPasswordInput } from '../../../libs/auth/dto/reset-password.input';
import { getRandomNumber } from '../../misc/functions';
import { AuthServiceInterface } from '../../../libs/auth/interfaces/auth.service.interface';
import { JwtHelper } from '../../../libs/auth/jwt.helper';
import { RuntimeException } from '@nestjs/core/errors/exceptions/runtime.exception';
import { RegisterableAuthServiceInterface } from '../../../libs/auth/interfaces/registerable.interface';
import { PasswordResettableAuthServiceInterface } from '../../../libs/auth/interfaces/password.resettable.interface';

@Injectable()
export class UserAuthService
  implements
    AuthServiceInterface,
    RegisterableAuthServiceInterface,
    PasswordResettableAuthServiceInterface
{
  constructor(
    private readonly jwtHelper: JwtHelper,
    private readonly prisma: PrismaService,
    private readonly passwordService: PasswordService,
    private readonly configService: ConfigService,
    private readonly notificationService: NotificationService,
  ) {}

  async register(payload: SignupInput): Promise<Token> {
    const hashedPassword = await this.passwordService.hashPassword(
      payload.password,
    );

    let email_verified_at: any = null;
    let is_email_verified: any = false;

    const appConfig = this.configService.get<AppConfig>('app');

    if (!appConfig.emailVerificationEnabled) {
      email_verified_at = new Date(Date.now()).toISOString();
      is_email_verified = true;
    }
    try {
      const user = await this.prisma.user.create({
        data: {
          ...payload,
          emailVerifiedAt: email_verified_at,
          isEmailVerified: is_email_verified,
          password: hashedPassword,
        },
      });
      return this.jwtHelper.generateToken({ authIdentifier: user.id });
    } catch (e) {
      if (
        e instanceof Prisma.PrismaClientKnownRequestError &&
        e.code === 'P2002'
      ) {
        throw new ConflictException(`Email ${payload.email} already used.`);
      } else {
        throw new Error(e);
      }
    }
  }

  async login({ username, password }): Promise<Token> {
    const user = await this.prisma.user.findFirst({
      where: {
        OR: [
          {
            username,
          },
          {
            email: username,
          },
        ],
      },
    });

    if (!user) {
      throw new NotFoundException(`No user found`);
    }

    const passwordValid = await this.passwordService.validatePassword(
      password,
      user.password,
    );

    if (!passwordValid) {
      throw new BadRequestException('Invalid password');
    }

    return this.jwtHelper.generateToken({ authIdentifier: user.id });
  }

  getUserByIdentifier(authIdentifier): Promise<User> {
    return this.prisma.user.findUnique({ where: { id: authIdentifier } });
  }

  getUserFromToken(token: string): Promise<User> {
    const { authIdentifier } = this.jwtHelper.authIdentifierFromToken(token);
    return this.prisma.user.findUnique({ where: { id: authIdentifier } });
  }

  refreshToken(token: string): Token {
    return this.jwtHelper.refreshToken(token);
  }

  async sendForgetPasswordNotification(payload): Promise<boolean> {
    try {
      const user = await this.prisma.user.findUnique({
        where: payload,
      });
      if (user) {
        const reset_code = getRandomNumber(6).toString();
        const data = {
          verification_code: reset_code,
        };
        await this.prisma.user.update({
          where: {
            id: user.id,
          },
          data: {
            resetCode: reset_code,
          },
        });
        this.notificationService.send(
          new MailVerificationNotification(data),
          user,
        );
        return true;
      } else {
        throw new NotFoundException('No User Found');
      }
    } catch (e) {
      throw new RuntimeException(e.message);
    }
  }

  async resetPassword(payload: ResetPasswordInput): Promise<boolean> {
    try {
      const user = await this.prisma.user.findFirst({
        where: {
          AND: {
            email: payload.email,
            resetCode: payload.code,
          },
        },
      });
      if (user) {
        const hashedPassword = await this.passwordService.hashPassword(
          payload.password,
        );
        await this.prisma.user.update({
          where: {
            id: user.id,
          },
          data: {
            password: hashedPassword,
            resetCode: null,
          },
        });
        return true;
      } else {
        throw new NotFoundException('Invalid code! Please try again');
      }
    } catch (e) {
      throw new RuntimeException(e.message);
    }
  }
}
