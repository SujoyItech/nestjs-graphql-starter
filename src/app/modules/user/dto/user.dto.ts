import {
  IsAlphanumeric,
  IsEmail,
  IsNotEmpty,
  Matches,
  MinLength,
} from 'class-validator';
import { InputType, Field } from '@nestjs/graphql';
import { Match } from '../../../../libs/decorators/match.decorator';

@InputType()
export class UpdateProfileInput {
  @Field()
  @IsAlphanumeric()
  username: string;

  @Field()
  @IsEmail()
  email: string;

  @Field({ nullable: true })
  name: string;

  @Field({ nullable: true })
  phone: string;

  @Field({ nullable: true })
  avatar: string;

  @Field({ nullable: true })
  resetCode: string;
}

@InputType()
export class UpdatePasswordInput {
  @Field()
  @IsNotEmpty()
  @MinLength(8)
  oldPassword: string;

  @Field()
  @IsNotEmpty()
  @MinLength(8)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'password too weak',
  })
  password: string;

  @Field()
  @IsNotEmpty()
  @MinLength(8)
  @Match('password')
  passwordConfirm: string;
}
