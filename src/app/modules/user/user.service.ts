import { Injectable } from '@nestjs/common';
import { MailVerificationNotification } from '../../notifications/mailverification.notification';
import { responseMessageWithStatus } from '../../models/dto/response-message-with-status';
import { NotificationService } from '../../../libs/notification/notification.service';
import { CacheService } from '../../../libs/cache/cache.service';
import { PrismaService } from 'nestjs-prisma';
import { UpdatePasswordInput, UpdateProfileInput } from './dto/user.dto';
import { compare } from 'bcryptjs';
import { PasswordService } from '../../../libs/auth/password.service';
import { getRandomNumber } from '../../misc/functions';

@Injectable()
export class UserService {
  constructor(
    private readonly mailVerificationNotification: MailVerificationNotification,
    private readonly notificationService: NotificationService,
    private readonly cacheService: CacheService,
    private readonly prisma: PrismaService,
    private readonly passwordService: PasswordService,
  ) {}

  async sendVerificationMail(user) {
    try {
      const verification_code = getRandomNumber(6);
      const data = {
        verification_code: verification_code,
      };
      const key = 'verification_code_' + user.id;
      await this.cacheService.put(key, verification_code);
      this.notificationService.send(
        new MailVerificationNotification(data),
        user,
      );
      return responseMessageWithStatus(true, 'Email send successfully');
    } catch (e) {
      return responseMessageWithStatus(false, e.message);
    }
  }

  async verifyUserEmail(code, user) {
    try {
      const key = 'verification_code_' + user.id;
      const verification_code = await this.cacheService.get(key);
      if (code === verification_code) {
        const updateUser = await this.prisma.user.update({
          where: {
            id: user.id,
          },
          data: {
            emailVerifiedAt: new Date(Date.now()).toISOString(),
            isEmailVerified: true,
          },
        });
        if (updateUser) {
          return responseMessageWithStatus(
            true,
            'Email verified successfully!',
          );
        } else {
          return responseMessageWithStatus(false, 'Email verify failed!');
        }
      } else {
        return responseMessageWithStatus(
          false,
          'Verification code not matched or expired!.',
        );
      }
    } catch (e) {
      return responseMessageWithStatus(false, e.message);
    }
  }

  async updateProfile(payload: UpdateProfileInput, user) {
    try {
      const response = await this.prisma.user.update({
        where: {
          id: user.id,
        },
        data: {
          ...payload,
        },
      });
      return response
        ? responseMessageWithStatus(true, 'Profile updated successfully!')
        : responseMessageWithStatus(false, 'Profile update failed.');
    } catch (e) {
      return responseMessageWithStatus(false, e.message);
    }
  }

  async updatePassword(payload: UpdatePasswordInput, user) {
    try {
      const validatePassword = await compare(
        payload.oldPassword,
        user.password,
      );
      if (validatePassword) {
        const hashedPassword = await this.passwordService.hashPassword(
          payload.password,
        );
        const response = await this.prisma.user.update({
          where: {
            id: user.id,
          },
          data: {
            password: hashedPassword,
          },
        });
        return response
          ? responseMessageWithStatus(true, 'Password updated successfully!')
          : responseMessageWithStatus(false, 'Password update failed.');
      } else {
        return responseMessageWithStatus(false, 'Incorrect old password!');
      }
    } catch (e) {
      return responseMessageWithStatus(false, e.message);
    }
  }
}
