import { Args, Float, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { UserEntity } from '../../../libs/decorators/user.decorator';
import { User } from '../../models/user.model';
import { UserService } from './user.service';
import { ResponseMessageWithStatusModel } from '../../models/dto/response-message-with-status.model';
import { UpdatePasswordInput, UpdateProfileInput } from './dto/user.dto';
import { GqlAuthGuard } from '../../../libs/auth/gql.auth.guard';

@Resolver(() => User)
@UseGuards(GqlAuthGuard())
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query(() => User)
  async me(@UserEntity() user: User): Promise<User> {
    return user;
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async updateProfile(
    @Args('data') data: UpdateProfileInput,
    @UserEntity() user: User,
  ) {
    return await this.userService.updateProfile(data, user);
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async updatePassword(
    @Args('data') data: UpdatePasswordInput,
    @UserEntity() user: User,
  ) {
    return await this.userService.updatePassword(data, user);
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async sendVerificationMail(@UserEntity() user: User) {
    return await this.userService.sendVerificationMail(user);
  }

  @Mutation(() => ResponseMessageWithStatusModel)
  async verifyUserEmail(@Args('code') code: number, @UserEntity() user: User) {
    return await this.userService.verifyUserEmail(code, user);
  }
}
