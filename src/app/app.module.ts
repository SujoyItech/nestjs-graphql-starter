import { Module } from '@nestjs/common';
import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/user/user.module';
import { StaffModule } from './modules/staff/staff.module';
import { MiscModule } from './modules/misc/misc.module';

@Module({
  imports: [AuthModule, MiscModule, UserModule, StaffModule],
})
export class AppModule {}
