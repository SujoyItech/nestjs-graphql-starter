"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.User = void 0;
var graphql_1 = require("@nestjs/graphql");
var base_model_1 = require("../../libs/model/base.model");
var User = /** @class */ (function (_super) {
    __extends(User, _super);
    function User() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    User._GRAPHQL_METADATA_FACTORY = function () {
        return { id: { type: function () { return Number; } }, username: { type: function () { return String; } }, email: { type: function () { return String; } }, name: { type: function () { return String; } }, phone: { nullable: true, type: function () { return String; } }, walletAddress: { nullable: true, type: function () { return String; } } };
    };
    __decorate([
        (0, graphql_1.HideField)()
    ], User.prototype, "walletPrivateKey");
    __decorate([
        (0, graphql_1.HideField)()
    ], User.prototype, "password");
    User = __decorate([
        (0, graphql_1.ObjectType)()
    ], User);
    return User;
}(base_model_1.BaseModel));
exports.User = User;
