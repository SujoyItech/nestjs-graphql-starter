import { ObjectType, HideField } from '@nestjs/graphql';
import { AuthenticatableInterface } from '../../libs/auth/authenticatable.interface';
import { BaseModel } from '../../libs/model/base.model';

@ObjectType()
export class User extends BaseModel implements AuthenticatableInterface {
  username: string;
  email: string;
  name: string;
  avatar?: string;
  phone?: string;
  emailVerifiedAt?: Date;
  isEmailVerified: boolean;
  resetCode?: string;
  @HideField()
  password: string;
}
