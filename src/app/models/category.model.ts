import { ObjectType } from '@nestjs/graphql';
import { BaseModel } from '../../libs/model/base.model';

@ObjectType()
export class Category extends BaseModel {
  id: number;
  title: string;
  image: string;
}
