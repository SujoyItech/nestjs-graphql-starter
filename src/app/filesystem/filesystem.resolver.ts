import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { FilesystemService } from './filesystem.service';
import { UserEntity } from '../../libs/decorators/user.decorator';
import { User } from '../models/user.model';
import { GraphQLUpload, FileUpload } from 'graphql-upload';
import { GraphQLError } from 'graphql';
import { FileObject } from './file.object';
import { GqlAuthGuard } from '../../libs/auth/gql.auth.guard';

@Resolver('FilesystemResolver')
@UseGuards(GqlAuthGuard())
export class FilesystemResolver {
  constructor(private readonly filesystemService: FilesystemService) {}

  @Query(() => [FileObject])
  async listFile(@UserEntity() user: User): Promise<Array<FileObject>> {
    return this.filesystemService.listFile(user);
  }

  @Mutation(() => FileObject)
  async uploadFile(
    @UserEntity() user: User,
    @Args({ name: 'file', type: () => GraphQLUpload }) upload: FileUpload,
  ): Promise<FileObject> {
    try {
      return await this.filesystemService.upload(upload, user);
    } catch (e) {
      throw new GraphQLError(e.message);
    }
  }
}
