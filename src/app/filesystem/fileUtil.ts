import { FileUpload } from 'graphql-upload';
import { randomUUID } from 'crypto';
import { ReadStream } from 'fs-capacitor';

export class FileUtil {
  private static allowedMimes = {
    image: ['image/png', 'image/jpeg', 'image/gif'],
    video: ['video/mp4', 'video/mpeg'],
  };
  private static allowedExts = {
    image: ['png', 'jpeg', 'jpg', 'gif'],
    video: ['mp4', 'm4a', 'm4p', 'm4b', 'm4r', 'm4v'],
  };
  public static imageSizes = {
    thumb: { width: 200, height: 200 },
  };
  constructor(private readonly file: FileUpload) {}

  static extension(name: string) {
    return name.split('.').pop();
  }

  extension(): string {
    return FileUtil.extension(this.file.filename);
  }

  mimeType(): string {
    return this.file.mimetype;
  }

  readStream(): ReadStream {
    return this.file.createReadStream();
  }

  hashName(): string {
    return randomUUID() + '.' + this.extension();
  }

  isValid(): boolean {
    return (
      Object.values(FileUtil.allowedExts).flat().includes(this.extension()) &&
      Object.values(FileUtil.allowedMimes).flat().includes(this.mimeType())
    );
  }

  static type(name: string) {
    for (const t in FileUtil.allowedExts) {
      if (FileUtil.allowedExts[t].includes(FileUtil.extension(name))) return t;
    }
    return undefined;
  }

  type(): string {
    return FileUtil.type(this.file.filename);
  }

  isImage(): boolean {
    return (
      FileUtil.allowedExts.image.includes(this.extension()) &&
      FileUtil.allowedMimes.image.includes(this.mimeType())
    );
  }

  isVideo(): boolean {
    return (
      FileUtil.allowedExts.video.includes(this.extension()) &&
      FileUtil.allowedMimes.video.includes(this.mimeType())
    );
  }
}
