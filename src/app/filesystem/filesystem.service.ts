import { Injectable } from '@nestjs/common';
import { InjectFilesystem } from '@filesystem/nestjs';
import { IFilesystemAdapter } from '@filesystem/core/interfaces';
import { ConfigService } from '@nestjs/config';
import { FileUpload } from 'graphql-upload';
import { IFilesystemModuleOptions } from '@filesystem/nestjs/interfaces/file-system-module-options';
import { User } from '../models/user.model';
import { FileUtil } from './fileUtil';
import { FileObject } from './file.object';
import { resolve } from 'path';
import { readdir } from 'fs/promises';
import { File } from 'nft.storage';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const sharp = require('sharp');

@Injectable()
export class FilesystemService {
  constructor(
    private readonly configService: ConfigService,
    @InjectFilesystem('public') private readonly publicDisk: IFilesystemAdapter,
  ) {}

  async listFile(user: User): Promise<Array<FileObject>> {
    const baseDir = `user-${user.id}`;
    const directory = resolve(`public/storage/${baseDir}`);
    try {
      const files = await readdir(directory);
      return files
        .filter((item) => {
          const reg = /[\w,\s-]+\.[A-Za-z0-9]{3}/g;
          return reg.test(item);
        })
        .map((item): FileObject => {
          const type = FileUtil.type(item);
          return {
            name: item,
            type: type,
            url: this.url(baseDir + '/' + item),
            variants:
              type == 'image'
                ? Object.keys(FileUtil.imageSizes).map((sizeKey) => {
                    return {
                      type: sizeKey,
                      url: this.url(baseDir + '/' + sizeKey + '/' + item),
                    };
                  })
                : null,
          };
        });
    } catch (e) {
      return [];
    }
  }

  async upload(upload: FileUpload, user: User): Promise<FileObject> {
    const userDir = `user-${user.id}`;
    const file = new FileUtil(upload);
    if (!file.isValid()) throw new Error('Invalid file');
    if (file.isImage()) return await this.resizeAndSave(file, userDir);
    const name = file.hashName();
    await this.publicDisk.writeStream(userDir + '/' + name, file.readStream());
    return {
      name: name,
      type: file.type(),
      url: this.url(userDir + '/' + name),
    };
  }

  url(path: string, diskName = 'public') {
    const filesystemConfig =
      this.configService.get<IFilesystemModuleOptions>('filesystem');
    const diskConfig = filesystemConfig.disks[diskName];

    if (diskName === 'public') {
      return diskConfig.url.trimEnd() + '/' + path.trimStart();
    }
    return undefined;
  }

  async getFileForNft(path: string, user: User) {
    const content = await this.publicDisk.read(`user-${user.id}/${path}`);
    return new File([content], path, { type: 'image/*' });
  }

  async resizeAndSave(file: FileUtil, baseDir = ''): Promise<FileObject> {
    if (!file.isImage()) throw new Error('Unable to resize non image file');
    let readStream = file.readStream();
    const name = file.hashName();
    const fileObject: FileObject = {
      name: name,
      type: 'image',
      url: null,
      variants: [],
    };

    await this.publicDisk.writeStream(baseDir + '/' + name, readStream);
    fileObject.url = this.url(baseDir + '/' + name);

    for (const sizeKey of Object.keys(FileUtil.imageSizes)) {
      readStream = await this.publicDisk.readStream(baseDir + '/' + name);
      await this.publicDisk.writeStream(
        baseDir + '/' + sizeKey + '/' + name,
        await readStream.pipe(sharp().resize(FileUtil.imageSizes[sizeKey])),
      );
      fileObject.variants.push({
        type: sizeKey,
        url: this.url(baseDir + '/' + sizeKey + '/' + name),
      });
    }

    return fileObject;
  }
}
