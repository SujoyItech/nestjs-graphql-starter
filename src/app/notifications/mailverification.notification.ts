import { NotificationInterface } from '../../libs/notification/notification.interface';
import { Type } from '@nestjs/common';
import { ChannelInterface } from '../../libs/notification/channels/channel.interface';
import { MailChannel } from '../../libs/notification/channels/mail.channel';
import { User } from '../models/user.model';
import { MessageInterface } from '../../libs/mail/messages/message.interface';
import { NotificationTemplate } from './notification.template';

export class MailVerificationNotification implements NotificationInterface {
  data: any;
  constructor(data) {
    this.data = data;
  }

  broadcastOn(): Type<ChannelInterface>[] {
    return [MailChannel];
  }

  toMail(notifiable: User): MessageInterface {
    return NotificationTemplate.toEmail('verification_mail', {
      name: notifiable.name,
      email: notifiable.email,
      verification_code: this.data.verification_code,
    }).to(notifiable.email);
  }

  queueable(): boolean {
    return false;
  }
}
