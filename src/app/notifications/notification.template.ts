import { MessageInterface } from '../../libs/mail/messages/message.interface';
import { MailMessage } from '../../libs/mail/messages/mail.message';
import { createWriteStream, readFileSync } from 'fs';
import { resolve } from 'path';
import { compile } from 'handlebars';

export class NotificationTemplate {
  static toEmail(template = 'default', variables: any): MessageInterface {
    const { subject, content } = NotificationTemplate.resolve(
      'email',
      template,
      variables,
    );
    return new MailMessage(content).subject(subject);
  }

  static update(channel: string, template: string, payload: any) {
    createWriteStream(
      resolve(`src/app/notifications/templates/${channel}/${template}.json`),
    ).write(JSON.stringify(payload));
  }

  static get(channel: string, template: string) {
    const templatePayload = readFileSync(
      resolve(`src/app/notifications/templates/${channel}/${template}.json`),
    ).toString();
    return JSON.parse(templatePayload);
  }

  static resolve(channel: string, template: string, variables: any) {
    const templateObj = NotificationTemplate.get(channel, template);
    const subject = compile(templateObj.subject)(variables);
    const content = compile(templateObj.template)(variables);
    return { subject, content };
  }
}
