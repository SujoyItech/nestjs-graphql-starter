import { NotificationInterface } from '../../libs/notification/notification.interface';
import { ConsoleChannel } from '../../libs/notification/channels/console.channel';
import { ChannelInterface } from '../../libs/notification/channels/channel.interface';
import { Type } from '@nestjs/common';
import { User } from '../models/user.model';
import { MailChannel } from '../../libs/notification/channels/mail.channel';
import { TwilioChannel } from '../../libs/notification/channels/twilio.channel';
import { NotificationTemplate } from './notification.template';
import { MessageInterface } from '../../libs/mail/messages/message.interface';

export class TestNotification implements NotificationInterface {
  broadcastOn(): Type<ChannelInterface>[] {
    // return [MailChannel, TwilioChannel, ConsoleChannel];
    return [MailChannel];
  }

  toMail(notifiable: User): MessageInterface {
    return NotificationTemplate.toEmail('default', { name: notifiable.name });
    // return `Hello! ${notifiable.username}`;
  }

  toTwilio(notifiable: User): string {
    return `Hello! ${notifiable.username}`;
  }

  queueable(): boolean {
    return true;
  }
}
