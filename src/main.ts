import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { MainModule } from './main.module';
import { ValidationPipe } from '@nestjs/common';
import { PrismaClientExceptionFilter } from 'nestjs-prisma';
import { ConfigService } from '@nestjs/config';
import { graphqlUploadExpress } from 'graphql-upload';
import { CorsConfig, NestConfig } from './configs/config.interface';

async function bootstrap() {
  const app = await NestFactory.create(MainModule);

  app.use(graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 10 })); // 10 mb limit

  // Validation
  app.useGlobalPipes(new ValidationPipe());

  // Prisma Client Exception Filter for unhandled exceptions
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));

  const configService = app.get(ConfigService);
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const nestConfig = configService.get<NestConfig>('nest');
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const corsConfig = configService.get<CorsConfig>('cors');

  // Cors
  if (corsConfig.enabled) {
    app.enableCors();
  }
  //
  await app.listen(process.env.PORT || nestConfig.port || 3000);
}

bootstrap();
