import { registerAs } from '@nestjs/config';
import { GraphQLError, GraphQLFormattedError } from 'graphql';
import { GraphqlConfig } from './config.interface';

export const GraphQLConfig = registerAs(
  'graphql',
  (): GraphqlConfig => ({
    playgroundEnabled: true,
    debug: true,
    schemaDestination: './graphql/schema.gql',
    sortSchema: true,
    formatError: (error: GraphQLError): GraphQLFormattedError => {
      // console.log(JSON.stringify(error));
      let message: string;
      let messages: string[];
      let code: number;
      try {
        if (error.extensions.hasOwnProperty('exception')) {
          message = error.message;
          code = error.extensions.exception.status || 500;
        } else {
          if (Array.isArray(error.extensions.response.message)) {
            messages = error.extensions.response.message;
            message = error.extensions.response.message[0];
          } else {
            message = error.extensions.response.message;
          }
          code = error.extensions.response.statusCode;
        }
        return errorObj(message, messages, code);
      } catch (e) {
        if (
          error.extensions.code === 'GRAPHQL_VALIDATION_FAILED' ||
          error.extensions.code === 'BAD_USER_INPUT'
        )
          return errorObj(error.message);
        else {
          console.error(JSON.stringify(error));
          return errorObj('Something went wrong.');
        }
      }
    },
  }),
);

function errorObj(message, messages?, code?) {
  return <any>{
    message: message,
    messages: messages ?? [],
    code: code || 500,
  };
}
