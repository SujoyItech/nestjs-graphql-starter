import { AppConfig as AppConfigInterface } from './config.interface';
import { registerAs } from '@nestjs/config';

export const AppConfig = registerAs(
  'app',
  (): AppConfigInterface => ({
    timeZone: process.env.TZ || 'Asia/Dhaka',
    emailVerificationEnabled: false,
    cur: 'ETH',
    adminCommission: 0.001,
    adminWalletAddress: null,
    sections: {
      category: {
        title: 'Browse by category',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis accumsan nisi Ut ut felis congue nisl hendrerit commodo.',
      },
    },
  }),
);
