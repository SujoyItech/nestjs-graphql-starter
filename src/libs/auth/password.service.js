"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.PasswordService = void 0;
var common_1 = require("@nestjs/common");
var bcryptjs_1 = require("bcryptjs");
var config_interface_1 = require("../../configs/config.interface");
var PasswordService = /** @class */ (function () {
    function PasswordService(configService) {
        this.configService = configService;
    }
    Object.defineProperty(PasswordService.prototype, "bcryptSaltRounds", {
        get: function () {
            var securityConfig = this.configService.get('security');
            var saltOrRounds = securityConfig.bcryptSaltOrRound;
            return Number.isInteger(Number(saltOrRounds))
                ? Number(saltOrRounds)
                : saltOrRounds;
        },
        enumerable: false,
        configurable: true
    });
    PasswordService.prototype.validatePassword = function (password, hashedPassword) {
        return (0, bcryptjs_1.compare)(password, hashedPassword);
    };
    PasswordService.prototype.hashPassword = function (password) {
        return (0, bcryptjs_1.hash)(password, this.bcryptSaltRounds);
    };
    PasswordService = __decorate([
        (0, common_1.Injectable)()
    ], PasswordService);
    return PasswordService;
}());
exports.PasswordService = PasswordService;
