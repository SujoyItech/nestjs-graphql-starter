import { Injectable } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { MailConfig } from '../../configs/config.interface';
import { transports } from './transports';
import { TransportInterface } from './transports/transport.interface';
import { MessageInterface } from './messages/message.interface';

@Injectable()
export class MailService {
  private mailConfig: MailConfig;
  constructor(
    private readonly moduleRef: ModuleRef,
    private readonly configService: ConfigService,
  ) {
    this.mailConfig = this.configService.get<MailConfig>('mail');
  }
  async send(message: MessageInterface) {
    const transport: TransportInterface = await this.resolveTransport();
    const data = await message.toTransporter();
    if (!data.from) {
      data.from = this.mailConfig.from;
    }
    return await transport.send(
      data,
      this.mailConfig.mailers[this.mailConfig.defaultMailer],
    );
  }
  resolveTransport() {
    return this.moduleRef.create(transports[this.mailConfig.defaultMailer]);
  }
}
