import { User } from '../../../app/models/user.model';
import { NotificationInterface } from '../notification.interface';

export interface ChannelInterface {
  send(notifiable: User, notification: NotificationInterface): Promise<void>;
}
