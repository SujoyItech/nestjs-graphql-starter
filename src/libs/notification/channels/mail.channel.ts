import { ChannelInterface } from './channel.interface';
import { User } from '../../../app/models/user.model';
import { NotificationInterface } from '../notification.interface';
import { MailService } from '../../mail/mail.service';
import { MessageInterface } from '../../mail/messages/message.interface';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailChannel implements ChannelInterface {
  constructor(private readonly mailService: MailService) {}

  send(notifiable: User, notification: NotificationInterface): Promise<any> {
    const mailMessage: MessageInterface = this.getData(
      notifiable,
      notification,
    );
    return this.mailService.send(
      mailMessage.to({
        name: notifiable.name,
        address: notifiable.email,
      }),
    );
  }

  private getData(
    notifiable: User,
    notification: NotificationInterface,
  ): MessageInterface {
    if (typeof notification['toMail'] === 'function') {
      return notification['toMail'](notifiable);
    }
    throw new Error('toMail method is missing into Notification class');
  }
}
