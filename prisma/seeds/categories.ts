export const categories = [
  {
    title: 'Wake Up Happy',
    image: 'https://i.scdn.co/image/ab67706f000000030bd6693bac1f89a70d623e4d',
  },
  {
    title: 'Morning Motivation',
    image: 'https://i.scdn.co/image/ab67706f00000003037da32de996d7c859b3b563',
  },
  {
    title: 'Walking On Sunshine',
    image: 'https://i.scdn.co/image/ab67706f000000035611e6effd70cdc11d0c7076',
  },
];
