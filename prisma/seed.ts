import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();

import { categories } from './seeds/categories';

async function main() {
  await prisma.category.createMany({
    data: categories,
  });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
