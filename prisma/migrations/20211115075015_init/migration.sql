-- CreateTable
CREATE TABLE "roles" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "permission" TEXT,

    CONSTRAINT "roles_pkey" PRIMARY KEY ("id")
);
