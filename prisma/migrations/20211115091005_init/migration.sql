/*
  Warnings:

  - You are about to drop the column `permission` on the `roles` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "roles" DROP COLUMN "permission",
ADD COLUMN     "permissions" TEXT;
